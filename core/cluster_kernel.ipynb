{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import escape as esc\n",
    "# to activate serialization of objects\n",
    "import escape.serialization as ser\n",
    "import numpy as np\n",
    "esc.require(\"0.9.7\")\n",
    "from escape.utils.widgets import show\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cluster kernel. Parallel computing using networked computers.\n",
    "\n",
    "If the multithreading supported by the kernel is not sufficient for your task, ESCAPE also supports parallelization using a cluster of computers connected via ethernet. In this case, your task will be divided among the computers, and each subtask on each computer will run in parallel, using by default all available CPU cores.\n",
    "\n",
    "The cluster can also run on a single PC, representing so-called multiprocessing parallelization.\n",
    "\n",
    "The current implementation of this feature uses the ipyparallel module and each PC on the network must be properly configured as follows:\n",
    "\n",
    "1. Install the same version of ESCAPE on all PCs along with the ipyparallel module, if it is not already installed.\n",
    "2. Read the documentation at https://ipyparallel.readthedocs.io/en/latest/tutorial/process.html#general-considerations to configure the cluster correctly. In this example, we use a cluster that normally runs on localhost. In order to distribute tasks among multiple PCs, you will have to configure and run the controller and engines yourself.\n",
    "3. In your notebook, import the serialization module as follows\n",
    "    ```python\n",
    "    import escape.serialization\n",
    "    ```\n",
    "    \n",
    "For this notebook, we have prepared two types of kernels: a simple and fast functor with vectorization over large arrays with millions of elements, and a slow functor with relatively small arrays with hundreds of elements.\n",
    "\n",
    "When cores are divided into subtasks, parts of the arrays are serialized and sent by the controller to the cluster engines before computation. When all subtasks are completed, the resulting arrays are assembled from engines. Serialization and deserialization along with transporting the serialized arrays over the network incurs the cost of performing i/o operations.\n",
    "\n",
    "Multithreading also has a performance cost due to the time it takes to run a single thread in a process. However, these costs can be reduced by using the so-called threadpool design pattern used by default in ESCAPE.\n",
    "\n",
    "In this notebook, we compare the computational speeds of multiprocessing and multithreading on a single PC and demonstrate how to run the kernel on multiple PCs. For our tests below, we used a multi-core PC with a 6-core Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz with 64 Gb RAM and a linux operating system. The processor uses hyperthreading, providing 12 threads.\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#creating a cluster with six nodes and connect a client\n",
    "import ipyparallel as ipp\n",
    "\n",
    "cluster = ipp.Cluster(n=16)\n",
    "cluster.start_cluster_sync()\n",
    "rc = cluster.connect_client_sync()\n",
    "rc.wait_for_engines(16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 1. \"Slow\" functor. Small arrays.\n",
    "\n",
    "The kernel below calculates at each point given by the elements of the `x` array the integral of the damped oscillatory function `F` over the variable frequency `b` with 61 quadrature points.\n",
    "\n",
    "The results are stored in the `y` array.\n",
    "\n",
    "The integration is adaptive, and the number of iterations required to achieve convergence differs from point to point.\n",
    "Thus, we expect some threads or processes to be completed earlier, and the total computation time for the whole range of values will be equal to the slowest parallel subtask."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = esc.var(\"X\")\n",
    "\n",
    "b = esc.par(\"b\", 5)\n",
    "a = esc.par(\"a\", 100 )\n",
    "c = esc.par(\"c\", 0.5)\n",
    "\n",
    "F = a*esc.pow(esc.cos(X*b), 2.0)*esc.exp(-c*X)\n",
    "I = esc.integral(F, b, 0, 500, numpoints=61, epsabs=1e-5, epsrel=1e-5, maxiter=300)\n",
    "#linspace return view of an array, \n",
    "#thus making a copy of it to avoid copying by the kernel\n",
    "x = np.linspace(-10, 10, 500, dtype=float).copy()\n",
    "y = np.zeros(x.shape, dtype=float)\n",
    "\n",
    "show(F, coordinates=x, title = 'F(X; a, b, c)')\n",
    "show(I, coordinates=x, title = 'I(X; a, c)')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# computation times for cluster and multithreading\n",
    "def compute_parallel(func, x, y):\n",
    "    #multithreading\n",
    "    nth = np.arange(1, 17, 1, dtype=int)\n",
    "    tms_mth = np.zeros(shape=nth.shape, dtype=float)\n",
    "    for i, n in enumerate(nth):\n",
    "        k = esc.kernel(\"Parallel threads\", func, numthreads=n)\n",
    "        t = %timeit -o -q k(x, y)\n",
    "        tms_mth[i] = t.average*1000\n",
    "    #multiprocessing single threads per node    \n",
    "    nrc1 = np.arange(1, 17, 1, dtype=int)\n",
    "    tms_rc_1 = np.zeros(shape=nrc1.shape, dtype=float)\n",
    "    for i, n in enumerate(nrc1):\n",
    "        k = esc.kernel(\"Cluster kernel\", func, multithreaded=False, rc=rc, rc_ids=range(0, n))\n",
    "        t = %timeit -o -q k(x, y)\n",
    "        tms_rc_1[i] = t.average*1000\n",
    "    #multiprocessing two threads per node\n",
    "    nrc2 = np.arange(1, 9, 1, dtype=int)\n",
    "    tms_rc_2 = np.zeros(shape=nrc2.shape, dtype=float)\n",
    "    for i, n in enumerate(nrc2):\n",
    "        k = esc.kernel(\"Cluster kernel\", func, numthreads=2, rc=rc, rc_ids=range(0, n))\n",
    "        t = %timeit -o -q k(x, y)\n",
    "        tms_rc_2[i] = t.average*1000\n",
    "    #multiprocessing three threads per node\n",
    "    nrc3 = np.arange(1, 6, 1, dtype=int)\n",
    "    tms_rc_3 = np.zeros(shape=nrc3.shape, dtype=float)\n",
    "    for i, n in enumerate(nrc3):\n",
    "        k = esc.kernel(\"Cluster kernel\", func, numthreads=3, rc=rc, rc_ids=range(0, n))\n",
    "        t = %timeit -o -q k(x, y)\n",
    "        tms_rc_3[i] = t.average*1000\n",
    "    return ((nth, tms_mth), (nrc1, tms_rc_1), (nrc2*2, tms_rc_2), (nrc3*3, tms_rc_3))\n",
    "\n",
    "def plot_data(data, title=\"\"):    \n",
    "    fig = plt.figure()\n",
    "    fig.suptitle(title)\n",
    "    ax = fig.add_subplot(111)\n",
    "\n",
    "    labels = [\"Multithreading\",\n",
    "              \"Cluster - 1 thread per node\",\n",
    "              \"Cluster - 2 thread per node\",\n",
    "              \"Cluster - 3 thread per node\",\n",
    "             ]\n",
    "    for i in range(0, 4):\n",
    "        ax.plot(data[i][0], data[i][1], 'o--', label=labels[i])\n",
    "\n",
    "    ax.set_xlabel(\"Number of used threads\")\n",
    "    ax.set_ylabel(\"Computation time [msec]\")\n",
    "\n",
    "    ax.legend()\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#single core\n",
    "print(\"Single core results\")\n",
    "k = esc.kernel(\"Single kernel\", I, multithreaded=False)\n",
    "t = %timeit -o k(x, y)    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = compute_parallel(I, x, y)\n",
    "plot_data(data, \"Slow function, small arrays (500 items)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 2. \"Fast\" functor, large arrays. \n",
    "\n",
    "The kernel below calculates functor `F` in every point given by array `x2` with 5x10⁶ items.\n",
    "The results are saved in array `y2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x2 = np.linspace(-10, 10, 5000000, dtype=float).copy()\n",
    "y2 = np.zeros(x2.shape, dtype=float)\n",
    "\n",
    "# simple kernel without parallel computation\n",
    "kf = esc.kernel(\"Single kernel\", F, multithreaded=False)\n",
    "%timeit kf(x2, y2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = compute_parallel(F, x2, y2)\n",
    "plot_data(data, \"Fast function, large arrays (5x10⁶ items)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interpretation of the results\n",
    "\n",
    "In our computational experiments the number of threads was taken to be more than twice the number of physical cores.\n",
    "It is known (https://ppc.cs.aalto.fi/ch3/hyperthreading/) that on processors with hyperthreading we can get up to 30% performance using all available threads.\n",
    "And to some extent we see the same improvements for examples with multithreading.\n",
    "\n",
    "In example 1, the multiprocessing speed is close to multi-threading, but splitting the array and collecting the results, of course, has its price\n",
    "as we can clearly see in the figure. Using fewer nodes and more threads per node improves the situation, but still the multi-threaded case is a winner.\n",
    "\n",
    "For large arrays and fast functions (example 2) we see that the multi-threaded case is even more preferable, the serialization and forwarding of large arrays definitely reduces the efficiency of multiprocessing. Using fewer nodes and more threads per node does not greatly improve computational speed. Here, multithreading is also the favorite. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cluster with two PCs\n",
    "\n",
    "Below we explore parallelization between two PCs, a 6-core ntel(R) Core(TM) i7-8700 CPU @ 3.20GHz and a 4-core Intel(R) Core(TM) i7-2600K CPU @ 3.40GHz.\n",
    "Both are located on the same WiFi network. \n",
    "\n",
    "We run a controller listening for all ip addresses on the first PC using the command `ipcontroller --ip=\"0.0.0.0\"`. This will also create configuration files \n",
    "usually located at `~/.ipython/profile_default/security/`, `ipcontroller-engine.json` for engines and `ipcontroller-client.json` for clients.\n",
    "These files must be copied to the same location on all computers running engines and clients.\n",
    "After copying the configuration files, we can start the engines on our PCs by typing the `ipengine` command.\n",
    "\n",
    "Read the documentation at https://ipyparallel.readthedocs.io/en/latest/tutorial/process.html#general-considerations for details and other possibilities for the cluster configuration.\n",
    "\n",
    "Below we run the same examples as above, with the slow integral functor `I` and the fast functor `F` for different sizes of the input array, starting with `500` elements and ending with `50000`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#connecting to the engines\n",
    "rc = ipp.Client()\n",
    "rc.wait_for_engines(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#kernels for testing\n",
    "#the fragmentation of arrays has been slightly corrected compared to the `rc_fragments='auto'`\n",
    "kc_I = esc.kernel(\"Cluster kernel\", I, rc=rc, rc_fragments=[0.7, 0.3])\n",
    "kn_I = esc.kernel(\"Multithreading kernel\", I)\n",
    "\n",
    "kc_F = esc.kernel(\"Cluster kernel\", F, rc=rc, rc_fragments=[0.7, 0.3])\n",
    "kn_F = esc.kernel(\"Multithreading kernel\", F)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tmsc_I, tmsn_I = [], []\n",
    "tmsc_F, tmsn_F = [], []\n",
    "for n in range(1, 100, 10):\n",
    "    x = np.linspace(-10, 10, 500*n, dtype=float).copy()\n",
    "    y = np.zeros(x.shape, dtype=float)\n",
    "    tc_I = %timeit -o -q -n 2 kc_I(x, y)\n",
    "    tn_I = %timeit -o -q -n 2 kn_I(x, y)\n",
    "    tc_F = %timeit -o -q -n 2 kc_F(x, y)\n",
    "    tn_F = %timeit -o -q -n 2 kn_F(x, y)\n",
    "    tmsc_I.append(tc_I.average*1000)\n",
    "    tmsn_I.append(tn_I.average*1000)\n",
    "    tmsc_F.append(tc_F.average*1000)\n",
    "    tmsn_F.append(tn_F.average*1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = np.arange(1, 100, 10)*500\n",
    "fig = plt.figure(figsize=(9, 4))\n",
    "ax = fig.add_subplot(121)\n",
    "ax.set_title(\"Slow function\")\n",
    "ax.plot(n, tmsc_I, 'o--', label=\"Cluster\")\n",
    "ax.plot(n, tmsn_I, 'o--', label=\"Multithreaded\")\n",
    "ax.set_xlabel(\"Length of input array\")\n",
    "ax.set_ylabel(\"Computation time [msec]\")\n",
    "ax.legend()\n",
    "ax = fig.add_subplot(122)\n",
    "ax.set_title(\"Fast function\")\n",
    "ax.plot(n, tmsc_F, 'o--', label=\"Cluster\")\n",
    "ax.plot(n, tmsn_F, 'o--', label=\"Multithreaded\")\n",
    "ax.set_xlabel(\"Length of input array\")\n",
    "ax.set_ylabel(\"Computation time [msec]\")\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results demonstrate that it is possible to get performance gain for a relatively slow function only.\n",
    "For the fast function the multithreading is preferable even for the large arrays.\n",
    "The splitting of arrays between nodes prior the computation of the fast function and collecting the results\n",
    "afterwards takes too long compared to the computation itself and doesn't give any advantage here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
