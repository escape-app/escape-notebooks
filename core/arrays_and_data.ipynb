{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import numpy as np\n",
    "import matplotlib.pylab as m\n",
    "\n",
    "import escape as esc\n",
    "from escape.utils.widgets import show\n",
    "esc.require(\"0.9.7\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Arrays wrapping\n",
    "\n",
    "In scattering, experimental data can be large. Depending on your sample and resolution of your instrument, single High Resolution X-ray diffraction curve measured near one diffraction peak can easily reach 5000 points. When developing library like ESCAPE one has to keep in mind a good enough performance in operating with large datasets, lack of unnecessary data copying to keep memory consumption constant and easy user interface to access and treat the data with other libraries. The *escape.core.array* module is responsible for that. This module provides an interface between numpy arrays and internal *array_t* core object which wraps a pointer to data and provides functionality to access this data by the core in an efficient way.\n",
    "\n",
    "This notebook demonstrates how wrapping of numpy arrays works with the following ESCAPE array types: *double_array_obj* and *mask_array_obj*. The latter works with bool arrays and used for data masking. User can create a wrapper which will manage the same memory as a source numpy array or a new object with a full copy of the source. \n",
    "\n",
    "All ESCAPE objects which require arrays for input and output do the wrapping automatically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we create two *double_array_obj* objects, one contains a copy of a numpy array, *dcopy* and a second which wraps the same numpy array, *dwrap* "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.zeros(5, dtype=float)\n",
    "\n",
    "dcopy = esc.double_array(x, copy=True)\n",
    "dwrap = esc.double_array(x, copy=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's set the last value of wrapper *dwrap* to 5, it shoud also change the original array. And we set *dcopy[0]* to -5, it doesn't influence original array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dwrap[1] = 5\n",
    "dcopy[1] = 10\n",
    "\n",
    "\n",
    "print(\"Copy:\", dcopy)\n",
    "print(\"Wrapper:\", dwrap)\n",
    "print(\"Original:\", x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Obviously, wrapping also works in another direction i.e. ESCAPE arrays *double_array_obj* and *mask_array_obj* can be wrapped by numpy library. In this case one can apply full functionality of numpy library to that arrays. Below we wrap a *dcopy* object. We set all its values to 10, which will not affect values of *dwrap* and *darr* arrays. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "npwrapper=np.asarray(dcopy)\n",
    "npwrapper[:]=20\n",
    "\n",
    "print(\"Copy:\", dcopy)\n",
    "print(\"Wrapper:\", dwrap)\n",
    "print(\"Original:\", x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Masked arrays work in the same way as double arrays, except source array should contain boolean values. Below is an example of wrapping of boolean array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#mask/boolean array\n",
    "\n",
    "m=np.ones(shape=5, dtype=bool)\n",
    "\n",
    "mwrap=esc.mask_array(m, copy=False)\n",
    "mcopy=esc.mask_array(m, copy=True)\n",
    "\n",
    "mwrap[1]=False\n",
    "mcopy[2]=False\n",
    "\n",
    "print (\"Copy: \", mcopy)\n",
    "print (\"Wrapper: \", mwrap)\n",
    "print (\"Original: \", m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data\n",
    "\n",
    "The object of *data_obj* type is a container class for experimental data. Data object is created with the following command:\n",
    "\n",
    "```python\n",
    "esc.data(name=\"Data name\", coordinates, experiment, errors=None, mask=None, \n",
    "          copy=False)\n",
    "```\n",
    "The *data* command expects 'coordinates' and 'experiment' arrays to have *numpy.ndarray* type. If not, they will be comverted and copied regardless of *copy* parameter.\n",
    "\n",
    "All arrays will be copied if 'copy' parameter is True, otherwise data_obj keeps wrappers of arrays if it is possible.\n",
    "If 'errors' parameter is *None*, the experimental errors will be calculated according to Poisson\n",
    "distribution, i.e. errors=sqrt(experiment). The 'mask' parameter is used if some data points should be hidden, for example, primary beam or cosmic rays.\n",
    "\n",
    "Data object has a `domain_size` property which returns number of coordinates in the input array, i.e. data which has domain size equal to $2$ correspond to a modeled function $F(x, y)$ and its input *coordinates* array is expected to have a form of $[x0, y0, x1, y1, x2, y2, ...]$. Two-dimensional arrays of the following form\n",
    "\n",
    "```python\n",
    "coordinates=[\n",
    "    [x0, y0],\n",
    "    [x1, y1],\n",
    "    [x2, y2]\n",
    "]\n",
    "```\n",
    "\n",
    "are also supported.\n",
    "\n",
    "Below few examples are given about how to create data objects of different domain size and how to plot them using *show* method from *escape.utils* module. In real life these arrays are read from experimental files of different formats. Currently there is no support of any format. The experimental data which is kept in ASCII text files can be read easily with exisitng numpy methods, like 'numpy.loadtxt', 'numpy.genfromtxt'.\n",
    "\n",
    "\n",
    "\n",
    "## Two-dimensional data\n",
    "\n",
    "Two-dimensional data is straightforward."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create abscissas array\n",
    "x=np.arange(-10, 10, 0.1)\n",
    "# create ordinates array of the same size\n",
    "y=1e3*np.sin(x)**2\n",
    "\n",
    "dobj=esc.data(\"2D Data\", x, y)\n",
    "\n",
    "# we use show method to plot the data\n",
    "show(dobj, title=\"2D data Plot\", xlabel=\"X\", ylabel=\"Y\", grid=True, xlog=True, ylog=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Two-dimensional data with mask"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create mask of the same size as the abscissas array\n",
    "mask = np.ones(shape=x.shape, dtype=bool)\n",
    "\n",
    "# masking all values in the range [-2.5, 2.5]\n",
    "mask[(x>-2.5) & (x<2.5)]=False\n",
    "\n",
    "mdobj=esc.data(\"2D Data with mask\", x, y, mask=mask)\n",
    "\n",
    "# we use show method to plot the data\n",
    "show(mdobj, title=\"2D Data with mask\", xlabel=\"X\", ylabel=\"Y\", grid=True, ylog=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Three-dimensional data\n",
    "\n",
    "Three-dimensional data has two coordinates $x, y$ and intensity along $z$ - axis. Below is an example of how coordinate and intensity arrays can be created. \n",
    "Show method supports currently only map-like plots for 3d data, which should be specified with *plot_type* parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we create first x and y one-dimensional equidistant arrays\n",
    "x=np.linspace(-5, 5, 50)\n",
    "y=np.linspace(-10, 10, 50)\n",
    "\n",
    "# create meshgrid of both coordinates\n",
    "xv, yv=np.meshgrid(x, y)\n",
    "\n",
    "# calculate 2d image of intensities\n",
    "z=np.sin(xv)**2*np.sin(yv)**2\n",
    "\n",
    "# flatten grid arrays\n",
    "xvf=xv.flatten()\n",
    "yvf=yv.flatten()\n",
    "\n",
    "# create coordinates array of type [[x0, y0], [x1, y1],...]\n",
    "xyf=np.column_stack([xvf, yvf]).flatten()\n",
    "# flatten intensities\n",
    "zf=z.flatten()\n",
    "\n",
    "dobj=esc.data(\"3D Data\", xyf, zf)\n",
    "\n",
    "# we use show method to plot the data\n",
    "show(dobj, figtitle=\"3D data Plot\", xlabel=\"X\", ylabel=\"Y\", plot_type=\"map\", rows=50)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Three-dimensional data with mask"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create mask of the same size as the abscissas array\n",
    "mask = np.ones(shape=z.shape, dtype=bool)\n",
    "\n",
    "# masking all values in the range [(-2.5, 2.5), (-2.5, 2.5)]\n",
    "mask[(xv>-2.5) & (xv<2.5) & (yv>-2.5) & (yv<2.5)]=False\n",
    "mask=mask.flatten()\n",
    "\n",
    "mdobj=esc.data(\"3D Data\", xyf, zf, mask=mask)\n",
    "\n",
    "# we use show method to plot the data\n",
    "show(mdobj, title=\"3D Data with mask\", xlabel=\"X\", ylabel=\"Y\", plot_type=\"map\", rows=50)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
